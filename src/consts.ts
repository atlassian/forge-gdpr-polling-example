export const POLLING_API_URL = "/app/report-accounts";
export const ACCOUNTS_TO_PROCESS_QUEUE_NAME = "processAccount";
export const QUEUE_PREFIX = 'queue:';
export const ACCOUNT_PREFIX = "account:";
export const POLL_BATCH_SIZE = 90;

/**
 * 
 * @param reqOrEvent first argument in function
 */
export const isWebTrigger = (reqOrEvent) => {
    if(reqOrEvent.method) {
        return true;
    }
    return false;
}

export const generateWebResponse = (statusCode, message) => {
    return {
        statusCode,
        body: JSON.stringify({message}),
        headers: {
            "content-type": ["application/json"]
        }
    }
}