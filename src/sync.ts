import { ACCOUNTS_TO_PROCESS_QUEUE_NAME, generateWebResponse, isWebTrigger, POLLING_API_URL, POLL_BATCH_SIZE } from './consts';
import { Account } from './accountMgmt';
import { Queue } from "./queue";
import { privacy } from '@forge/api';

export const run = async () => {  
    const accountList = await Account.getAllAccounts();
    let polledAccounts;
    try {
      polledAccounts = await pollAccounts(accountList);
    } catch (e) {
      console.error(e);
      return generateWebResponse(500, 'Invalid response from API')
    }
    const processingQueue = new Queue(ACCOUNTS_TO_PROCESS_QUEUE_NAME);
    await processingQueue.setQueue(polledAccounts);
    console.log('Generated processing queue');
};

export const runWeb = async (req) => {
  const isWeb = isWebTrigger(req);
  await run();
  if(isWeb) {
    return generateWebResponse(200, "Generated processing queue");
  }
}

// Replace with Privacy Polling API
const pollAccounts = async (accountList) => {
  const batches = [];
  for(let i = 0; i < accountList.length; i += POLL_BATCH_SIZE) {
    const chunk = accountList.slice(i, i + POLL_BATCH_SIZE);
    batches.push(chunk);
  }

  let polledAccounts = [];
  const responseAccounts = await Promise.all(batches.map(async (batch) => {
    if(batch.length === 0) {
      // Handle empty.
      return [];
    }
    const accounts = await privacy.reportPersonalData(batch.map(({ value: {accountId, updatedAt}}) => {return {updatedAt, accountId}}));
    return accounts;
  }));
  polledAccounts = responseAccounts.reduce((acc, val) => acc.concat(val), []); // flatten
  return polledAccounts;
}

interface PollResponse {
  accountId: string;
  status: "closed" | "updated";
}