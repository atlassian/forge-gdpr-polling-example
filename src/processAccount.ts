import { Account } from "./accountMgmt";
import { ACCOUNTS_TO_PROCESS_QUEUE_NAME, generateWebResponse, isWebTrigger } from "./consts"
import { handleWebQueue, Queue } from "./queue"

export const run = async () => {
    const processingQueue = new Queue(ACCOUNTS_TO_PROCESS_QUEUE_NAME);

    const accountOp = await processingQueue.peek();
    if(!accountOp) {
        // If nothing was on the queue, abort
        return;
    }

    const {accountId, status} = accountOp;
    const account = new Account(accountId);

    switch(status) {
        case 'updated':
            await account.processAccountUpdate();
            break;
        case 'closed':
            await account.processAccountDelete();
            break;
        default:
            throw new Error('Unsupported account operation - ' + status);
    }

    // Remove from queue to mark as processed
    await processingQueue.pop();
}

// Remove once no longer needed for debugging during development.
export const runWeb = async (req) => {
    const isWeb = isWebTrigger(req);
    if(isWeb) {
        // Web CRUD API for queue
        const processingQueue = new Queue(ACCOUNTS_TO_PROCESS_QUEUE_NAME);
        const response = await handleWebQueue(req, processingQueue);
        if(response) {
            return response;
        }
    }
    // If not handle by CRUD API, run normal trigger.
    await run();
    if(isWeb) {
        return generateWebResponse(204, "");
    }
}