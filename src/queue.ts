import { storage } from "@forge/api";
import { QUEUE_PREFIX, generateWebResponse } from "./consts";

export class Queue {
    private essQueueKey: string;

    constructor(private queueName: string) {
        this.essQueueKey = QUEUE_PREFIX + queueName;
    }

    public async getQueue() {
        return await storage.get(this.essQueueKey);
    }

    public async setQueue(queueList: any[]) {
        await storage.set(this.essQueueKey, queueList);
    }

    public async peek() {
        const queueList = await this.getQueue();
        if(!queueList) {
            // Deal with non-present queue
            await this.setQueue([]);
            return undefined;
        }
        if(queueList.length === 0) {
            return undefined;
        }
        return queueList[0];
    }

    public async pop() {
        const queueList = await this.getQueue();
        await storage.set(this.essQueueKey, queueList.slice(1));
        return queueList[0];
    }

    public async push(item) { 
        let queueList = await this.getQueue();
        if(!queueList) {
            queueList = [];
        }
        await this.setQueue([...queueList, item]);
    }

    public async clear() {
        await storage.delete(this.essQueueKey);
    }
}

export const handleWebQueue = async (req, queue: Queue) => {
    if(req.method === "POST") {
        const body = JSON.parse(req.body);
        const update = body.update;
        if(update) {
            console.log("Adding to list manually")
            await queue.push(update);
        }
        return generateWebResponse(201, "Added");
    } else if (req.method === "DELETE") {
        await queue.clear();
        return generateWebResponse(204, "Deleted data");
    } else if (req.method === "PUT") {
        const queueList = await queue.getQueue();
        console.log("Data: " + queueList);
        return generateWebResponse(200, JSON.stringify(queueList));
    }
}
