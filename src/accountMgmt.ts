import api, {startsWith, storage} from '@forge/api';
import { ACCOUNT_PREFIX } from './consts';

/**
 * Account management class, used to abstract away all account
 * related operations including storage, updates and deletion.
 */
export class Account {
    private storageKey: string;
    constructor(private accountId: string) {
        this.storageKey = ACCOUNT_PREFIX + accountId;
    }

    public async getStoredEntity(): Promise<StoredAccount> {
        return await storage.get(this.storageKey);
    }

    public async getJiraUser(): Promise<JiraUser> {
        const res = await api.asApp().requestJira(`/rest/api/3/user?accountId=${this.accountId}`);
        if(res.status !== 200) {
            throw new Error("Jira User not found - " + await res.text());
        }
        return await res.json();
    }

    private async deleteUser() {
        await storage.delete(this.storageKey);
    }

    private async updateStoredEntity(updateData: Partial<StoredAccount>){
        const user = await this.getStoredEntity();
        const newData = {
            ...user,
            ...updateData,
            updatedAt: (new Date()).toISOString()
        };
        await this.setStoredEntity(newData);
    }

    private async setStoredEntity(user: StoredAccount) {
        await storage.set(this.storageKey, {
            ...user,
            updatedAt: (new Date()).toISOString()
        });
    }
    
    public static async getAllAccounts() {
        const accountList = [];
        const query = storage
            .query()
            .where('key', 
                startsWith(ACCOUNT_PREFIX))
            .limit(20);

        let queryResponse =  await query.getMany();

        // Enumerate through all the results.
        while(queryResponse) {
            if(queryResponse) {
                accountList.push(...queryResponse.results);
            }

            // Continue while there is a cursor available.
            if(queryResponse.nextCursor) {
                queryResponse = await query.cursor(queryResponse.nextCursor).getMany();
            } else {
                queryResponse = undefined;
            }
        }

        return accountList.filter(item => item);
    }

    public async loadUser() {
        const {displayName, emailAddress} = await this.getJiraUser();
        await this.setStoredEntity({
            accountId: this.accountId,
            displayName,
            emailAddress,
            references: []
        });
    }

    public async processAccountUpdate() {
        const {displayName, emailAddress} = await this.getJiraUser();
        await this.updateStoredEntity({
            displayName,
            emailAddress
        });
        const storedUser = await this.getStoredEntity();
        if(storedUser.references) {
            await Promise.all(storedUser.references.map(this.updateReference));
        }
    }

    public async processAccountDelete() {
        const storedUser = await this.getStoredEntity();
        if(storedUser)  {
            if(storedUser.references) {
                // If we can't delete all references, then fail chain.
                await Promise.all(storedUser.references.map(this.deleteReference));
            }
            await this.deleteUser();
        }
    }

    private async updateReference(ref) {}
    private async deleteReference(ref) {}
}

export interface JiraUser {
    self: string,
    accountId: string,
    accountType: "atlassian" | "app" | "customer" | "unknown",
    emailAddress?: string,
    avatarUrls: {},
    displayName: string,
    active: boolean,
    timeZone?: string,
    locale?: string,
    groups: {},
    applicationRoles: {},
    expand: string
}

export interface StoredAccount {
    references: string[];
    accountId: string;
    displayName: string;
    emailAddress: string;
    updatedAt?: string;
}