import { storage } from '@forge/api';
import {Account} from './accountMgmt';
import { generateWebResponse } from './consts';

export const run = async (req) => {
    switch (req.method) {
        case "POST":
            // Create data entry for given accountId
            const body = JSON.parse(req.body);
            const accountId = body.account;
            console.log("AccountId: "+ accountId);
            const account = new Account(accountId);
            await account.loadUser();
            return generateWebResponse(201, 'Created account');
        case "DELETE":
            // Delete everything
            const everything = await getEverything();
            await Promise.all(everything.map(async ({key}) => await storage.delete(key)));
            return generateWebResponse(204, 'Deleted data');
        case "GET":
            // Show all data
            console.log('Everything: ' + JSON.stringify(await getEverything()));
            return generateWebResponse(200, '');
    }
}

const getEverything = async () => {
    const items = [];
    let queryResponse = await storage.query().getMany();
    while(queryResponse) {
        items.push(...queryResponse.results.map(({key, value}) => { return { key, value }}));
        if(queryResponse.nextCursor) {
            queryResponse = await storage.query().cursor(queryResponse.nextCursor).getMany();
        } else {
            queryResponse = undefined;
        }
    }
    return items;
}