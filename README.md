# Forge GDPR Polling Example

This project contains a Forge app written in Typescript that processes GDPR.

See [developer.atlassian.com/platform/forge/user-privacy-guidelines](https://developer.atlassian.com/platform/forge/user-privacy-guidelines/) 
for documentation and tutorials explaining Forge GDPR.

Please note - this app currently uses a mock implementation of the reporting API in `src/consts.ts`. This is for testing purposes, 
and should be replaced with a new Privacy API which will be published soon.

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

### Notes
- Use the `forge deploy` command when you want to persist code changes.
- Use the `forge install` command when you want to install the app on a new site.
- Use the `forge webtrigger` command to generate webtrigger URLs.
- Once the app is installed on a site, the site picks up the new app changes you deploy without needing to rerun the install command.

## Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.